# resource "random_string" "number" {
#   length = 8
#   special = false
#   number = true
#   upper = false
#   lower = false
# }

resource "google_compute_firewall" "allow-ssh" {
  name    = "allow-ssh"
  network = google_compute_network.graphite.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  target_tags = ["allow-ssh"]
}

resource "google_compute_firewall" "allow-http" {
  name    = "allow-http"
  network = google_compute_network.graphite.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  target_tags = ["allow-http"]
}

resource "google_compute_network" "graphite" {
  name = "graphite-network"
}