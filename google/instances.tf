resource "google_compute_instance" "graphite" {
  # gcp_instance_name is a list type variable.
  count = length(var.gcp_instance_name)
  # simple example using an array to name instances.
  name = var.gcp_instance_name[count.index]
  # example using a map type variable
  machine_type = var.environment == "production" ? var.gcp_machine_type["prod"]: var.gcp_machine_type["dev"]
  zone = var.gcp_zone
  can_ip_forward = false
  description = "Base template for instance."
  
  # Firewall rules.
  tags = [
    "allow-ssh",
    "allow-http"
  ]

  boot_disk {
    auto_delete = false
    initialize_params {
      image = var.environment == "production" ? var.gcp_image_type["prod"]: var.gcp_image_type["dev"]
      size = "20"
    }
  }

  labels = {
    name = var.gcp_instance_name[count.index]
    machine_type = var.environment == "production" ? var.gcp_machine_type["prod"]: var.gcp_machine_type["dev"]
  }

  metadata = {
    size = "20"
    test = "testing"
  }

  metadata_startup_script = "echo hello"

  network_interface {
    network = "graphite-network"
    access_config {
    }
  }

  provisioner "file" {
    source      = "scripts/provision.sh"
    destination = "/tmp/provision.sh"

    # connection {
    #   type = "ssh"
    #   user = "graphite"
    #   private_key = "${file("./creds/graphite")}"
    #   agent = "false"
    #   host = google_compute_instance.network_interface.*.access_config.0.nat_ip
    # }
  }

  provisioner "local-exec" {
    command = "sudo chmod +x /tmp/provision.sh; sudo /tmp/provision.sh"
  }

  scheduling {
    automatic_restart = true
  }
}


resource "google_compute_disk" "storage" {
  name = "graphite-storage"
  type = "pd-ssd"
  zone = var.gcp_zone
  size = "20"
}

resource "google_compute_attached_disk" "graphite-storage" {
  disk = google_compute_disk.storage.self_link
  instance = google_compute_instance.graphite[0].self_link
}

resource "google_storage_bucket" "bucket" {
  count = 1
  name = var.bucket_name
  location = var.gcp_zone
  storage_class = var.storage_class
  force_destroy = true
  
  lifecycle_rule {
    condition {
      age = 3
    }
    action {
      type = "Delete"
    }
  }

  labels = {
    name = var.bucket_name
    location = var.bucket_location
  }

  versioning {
    enabled = true
  }
}
