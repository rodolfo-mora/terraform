# output: Used to print out instance information
output "name" { value = google_compute_instance.graphite.*.name }
output "type" { value = google_compute_instance.graphite.*.machine_type }
output "zone" { value = google_compute_instance.graphite.*.zone }
# example using a joint statement
output "instance_id" { value = join(",", google_compute_instance.graphite.*.instance_id) }
