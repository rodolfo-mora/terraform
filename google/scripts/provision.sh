#!/bin/bash
apt update
apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common git zookeeper golang netcat vim
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt install -y docker-ce docker-compose
apt install -y collectd collectd-utils