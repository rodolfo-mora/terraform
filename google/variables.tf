variable "environment" {
  default = "production"
}

variable "machine_type" {
  default = "dev"
}

variable "gcp_credentials_file" {
  description = "Path to credentials file in string format."
  type = string
  default="credentials.json"
}

variable "gcp_project_name" {
  description = "Project name"
  type = string
  default = "terraform-gcp-309517"
}

variable "gcp_region" {
  description = "GCP Region used for the build"
  type = string
  default = "us-central1"
}

variable "gcp_instance_count" {
  default = 2
}

variable "gcp_instance_name" {
  type = list(string)
  default = [
    "graphite-1",
    "graphite-2"
  ]
}

data "google_compute_image" "ubuntu-1804" {
  family  = "debian-9"
  project = "debian-cloud"
}

data "google_compute_image" "ubuntu-2004" {
  family  = "debian-9"
  project = "debian-cloud"
}

variable "gcp_image_type" {
  type = map
  default ={
    "dev": "ubuntu-1804-lts",
    "prod": "ubuntu-2004-lts"

  }
}

variable "gcp_zone" {
  description = ""
  type = string
  default = "us-central1-c"
}

variable "gcp_machine_type" {
  description = "Type / Size of GCP machine"
  type = map
  default = {
    "dev": "n1-standard-1",
    "prod": "n1-standard-2"
  }
}

variable "gcp_network_name" {
  description = "Network used for GCP instances"
  type = string
  default = "terraform-test-network"
}

# Buckets section
variable "bucket_name"{
  description = "Default bucket"
  type = string
  default = "test-bucket"
}

variable "storage_class" {
  type = string
  default = "REGIONAL"
}

variable "bucket_location" {
  type = string
  default = "us-central1-c"

}



